create table passed_students(
student_id int references students(id),
exam_id int references exams(id),
score int not null);