package com.dmitryol;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
/**
 * Controller for edit exam
 * @author Dmitry
 *
 */
public class ExamEditController {

	@FXML
	private TextField subject;
	@FXML
	private Button ok;
	@FXML
	private ComboBox<String> day, month, year;
	
	private static Exam selectedExam;


	
	@FXML
	private void editExam(){
		Controller.database.editExam(subject.getText(), getDate(), selectedExam.getId());
		Controller.initExam();
		Controller.examEdit.hide();
	}
	@FXML
	private void initialize(){
		subject.setText(selectedExam.getSubject());
		day.getItems().addAll(Controller.getDay());
		month.getItems().addAll(Controller.getMonth());
		year.getItems().addAll(Controller.getYear());

	}
	/**
	 * Method gets selected exam
	 * @param exam - Selected exam
	 */
	public static void init(Exam exam){
		selectedExam=exam;
	}
	private String getDate(){
		return day.getValue()+"-"+month.getValue()+"-"+year.getValue();
	}
}
