package com.dmitryol;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
/**
 * Controller for edit student
 * @author Dmitry
 *
 */
public class StudEditController {
	
	@FXML
	private TextField name;
	@FXML
	private TextField surname;
	@FXML
	private Button ok;
	@FXML
	private ComboBox<String> group;
	private static Student selectedStudent;


	
	@FXML
	private void editStudent(){
		Controller.database.editStudent(name.getText(), surname.getText(), Controller.database.getIdGroup(group.getValue()), selectedStudent.getId());
		Controller.initStud();
		Controller.studEdit.hide();
	}
	@FXML
	private void initialize(){
		name.setText(selectedStudent.getName());
		surname.setText(selectedStudent.getSurname());
		group.getItems().addAll(Controller.getGroups());
	}
	/**
	 * Method gets selected student
	 * @param student - Selected student
	 */
	public static void init(Student student){
		selectedStudent=student;
	}
	
}
