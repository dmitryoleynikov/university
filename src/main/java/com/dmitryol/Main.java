package com.dmitryol;

import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class of the program.
 * @author Dmitry
 */

public class Main extends Application {
	
	/**
	 * Pool for database.
	 */
	protected static ComboPooledDataSource pool= new ComboPooledDataSource();
	/**
	 * Path to properties file.
	 */
	private static final String path="src/main/resources/university.properties";
	private static Properties config;
	
	public static void main(String[] args) {
		// Load data from properties file
		getProperties();
		// Setting pool for database
		settingPool();
		// Method class of the Application
		launch(args);

	}
	/**
	 * Abstract method class of the Application.
	 */
	@Override
	public void start(Stage Stage) throws Exception {
		//loading fxml file
		Parent root = FXMLLoader.load(getClass().getResource("university.fxml"));
		// Creating scene 
		Scene scene= new Scene(root, 600, 400);
		
		Stage.setScene(scene);
		Stage.setTitle("University");
		Stage.show();
		
	}
	/**
	 *  Setting pool for database.
	 */
	private static void settingPool(){
		try {
			pool.setDriverClass(config.getProperty("driver"));
			pool.setJdbcUrl(config.getProperty("url"));
			pool.setUser(config.getProperty("username"));
			pool.setPassword(config.getProperty("password"));
			pool.setMinPoolSize(1);
			pool.setAcquireIncrement(1);
			pool.setMaxPoolSize(25);
		} catch (PropertyVetoException e) {
			System.out.println("Pool error");
			e.printStackTrace();
		}
	}
	/**
	 * Loading configuration data from properties file for connection to database.
	 */
	private static void getProperties(){
		try {
			FileInputStream stream = new FileInputStream(path);	
		    config = new Properties();
			config.load(stream);
		
		}
		 catch (IOException e) {
			e.printStackTrace();
		}
	}
		

}
