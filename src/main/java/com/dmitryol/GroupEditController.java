package com.dmitryol;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
/**
 * Controller for edit group
 * @author Dmitry
 *
 */
public class GroupEditController {

	@FXML
	private TextField title;
	@FXML
	private Button ok;
	
	private static Group selectedGroup;


	
	@FXML
	private void editGroup(){
		Controller.database.editGroup(title.getText(), selectedGroup.getId());
		Controller.initStud();
		Controller.initGroup();
		Controller.groupEdit.hide();
	}
	@FXML
	private void initialize(){
		title.setText(selectedGroup.getTitle());

	}
	/**
	 * Method gets selected group
	 * @param group - Selected group
	 */
	public static void init(Group group){
		selectedGroup=group;
	}
	
}

