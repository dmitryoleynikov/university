package com.dmitryol;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
/**
 * The window for edits groups
 * @author Dmitry
 *
 */
class WindowGroupEdit {
	
private static final String RES="edit_group.fxml";
	
	private WindowGroupEdit(){}
		
	public static Stage createGroupEdit(){
		
		
		Stage stage=null;
		try{
			stage = new Stage();
			
			AnchorPane root = FXMLLoader.load(WindowStudEdit.class.getResource(RES));
			Scene scene = new Scene(root);
			
			stage.setScene(scene);
			stage.setTitle("Edit Group");
			
		}catch(IOException e){
			System.out.println("File "+RES+" not found");
			}
		return stage;
		
	}


}
