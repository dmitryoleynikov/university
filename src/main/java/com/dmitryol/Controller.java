package com.dmitryol;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
/**
 * Class controller. 
 * @author Dmitry
 */
public class Controller {
	  
	  protected static ObservableList<Student> studData = FXCollections.observableArrayList();
	  protected static ObservableList<Group> groupData = FXCollections.observableArrayList();
	  protected static ObservableList<Exam> examData = FXCollections.observableArrayList();
	  protected static DatabaseReader database= new DatabaseReader();
	  
	  protected static Stage studEdit=null;
	  protected static Stage groupEdit=null;
	  protected static Stage examEdit=null;
	  
	  @FXML
	  private TextField firstName, secondName, groupTitle, examTitle;
	  @FXML
	  private ComboBox<String> group, year, month, day;
	  @FXML
	  private Button addStudent, addGroup, addExam;
	  @FXML
	  // Table students.
	  private TableView<Student> students;
	  @FXML
	  private TableColumn<Student, Integer> studId; 
	  @FXML
	  private TableColumn<Student, String> studName; 
	  @FXML
	  private TableColumn<Student, String> studSurname; 
	  @FXML
	  private TableColumn<Student, String> number_group;
	  // Table groups.
	  @FXML
	  private TableView<Group> groups;
	  @FXML
	  private TableColumn<Group, Integer> groupId; 
	  @FXML
	  private TableColumn<Group, String> groupName; 
	  // Table exams.
	  @FXML
	  private TableView<Exam> exams;
	  @FXML
	  private  TableColumn<Exam, Integer> examId; 
	  @FXML
	  private TableColumn<Exam, String> examName; 
	  @FXML
	  private TableColumn<Exam, String> examDate; 
	  //Creating contextMenu for students
	  private ContextMenu contextStud = new ContextMenu();
	  private MenuItem changeStud = new MenuItem("Change");
	  private MenuItem deleteStud = new MenuItem("Delete");
	  //Creating contextMenu for groups
	  private ContextMenu contextGroup = new ContextMenu();
	  private MenuItem changeGroup = new MenuItem("Change");
	  private MenuItem deleteGroup = new MenuItem("Delete");
	  //Creating contextMenu for exams
	  private ContextMenu contextExam = new ContextMenu();
	  private MenuItem changeExam = new MenuItem("Change");
	  private MenuItem deleteExam = new MenuItem("Delete");
	  /**
	   * The method uses for initialize data in javaFX.
	   */
	   @FXML
	   private void initialize(){
	    	initStud();
	    	initGroupBox();
	    	initGroup();
	        initExam();
	        year.getItems().addAll(getYear());
	        month.getItems().addAll(getMonth());
	        day.getItems().addAll(getDay());
	    	// Setting each column table students in according POJO student.
	    	studId.setCellValueFactory(new PropertyValueFactory<Student, Integer>("id"));
	    	studName.setCellValueFactory(new PropertyValueFactory<Student, String>("name"));
	    	studSurname.setCellValueFactory(new PropertyValueFactory<Student, String>("surname"));
	    	number_group.setCellValueFactory(new PropertyValueFactory<Student, String>("group"));
	    	// Table groups.
	    	groupId.setCellValueFactory(new PropertyValueFactory<Group, Integer>("id"));
	    	groupName.setCellValueFactory(new PropertyValueFactory<Group, String>("title"));
	    	// Table exams.
	    	examId.setCellValueFactory(new PropertyValueFactory<Exam, Integer>("id"));
	    	examName.setCellValueFactory(new PropertyValueFactory<Exam, String>("subject"));
	    	examDate.setCellValueFactory(new PropertyValueFactory<Exam, String>("date"));
	    	// Setting data source for tables.
	    	students.setItems(studData);
	    	groups.setItems(groupData);
	        exams.setItems(examData);
	        // Adding ContextMenu for students
	        contextStud.getItems().addAll(changeStud, deleteStud);  
	        deleteStud.setOnAction(event ->{deleteStudent();});
	        changeStud.setOnAction(event ->{editStudent();});
	        students.setRowFactory(param -> {
	        	TableRow<Student> row = new TableRow<>();
	        	row.contextMenuProperty().setValue(contextStud);
	        	return row;
	        });
	        
	        //Adding ContetMenu for groups
	        contextGroup.getItems().addAll(changeGroup, deleteGroup);  
	        deleteGroup.setOnAction(event ->{deleteGroup();});
	        changeGroup.setOnAction(event ->{editGroup();});
	        groups.setRowFactory(param -> {
	        	TableRow<Group> row = new TableRow<>();
	        	row.contextMenuProperty().setValue(contextGroup);
	        	return row;
	        });
	    	
	        //Adding ContetMenu for exams
	        contextExam.getItems().addAll(changeExam, deleteExam);  
	        deleteExam.setOnAction(event ->{deleteExam();});
	        changeExam.setOnAction(event ->{editExam();});
	        exams.setRowFactory(param -> {
	        	TableRow<Exam> row = new TableRow<>();
	        	row.contextMenuProperty().setValue(contextExam);
	        	return row;
	        });
	    }
	   /**
	    * Filling data ObservableList for table students.
	    */
	  @FXML
	   protected static void initStud(){
		  // Clearing data before initialization.
		  studData.clear();
		   // Amount students in database.
	       int a=database.getStudets().size();
		   for(int i=0;i<a;i++){
			   studData.add(database.getStudets().get(i));
			   
	    }
	   }
	  /**
	   * Getting list groups for students.
	   * @return String[] of the groups.
	   */
	  @FXML
	  protected static String[] getGroups(){
		  // Amount groups in database.
		  int size=database.getGroups().size();
		  String [] groups= new String [size];
		  for(int i=0; i<size; i++){
			  groups[i]=database.getGroups().get(i).getTitle();
		  }
		  return groups;
	  }
	  /**
	   * Adding student into database. 
	   */
	  @FXML
	  private void addStudent(){
		  database.addStudent(firstName.getText(), secondName.getText(), database.getIdGroup(group.getValue()));
		  // Update data in the table.
		  initStud();
		  // Reset TextFields.
		  firstName.setText("");
		  secondName.setText("");
		  
	  }
	  /**
	    * Filling data ObservableList for table groups.
	    */
	  @FXML
	  protected static void initGroup(){  
		  groupData.clear();
		  int size=database.getGroups().size();
		  for(int i=0;i<size;i++){
			  groupData.add(database.getGroups().get(i));
		  }
		  
	  }
	  private void initGroupBox(){
		  group.getItems().clear();
		  group.getItems().addAll(getGroups());
	  }
	  /**
	   * Adding group of the students into database. 
	   */
	  @FXML
	  private void addGroup(){
		  database.addGroup(groupTitle.getText());
		  // Update data in the table.
		  initGroup();
		  groupTitle.setText("");
		  // Update ComboBox group.
		  initGroupBox();
	  }
	  /**
	    * Filling data ObservableList for table exams.
	    */
	  @FXML
	  protected static void initExam(){
		  examData.clear();
		  int size=database.getExams().size();
		  for(int i=0;i<size;i++){
			  examData.add(database.getExams().get(i));
		  }
	  }
	  /**
	   * Adding exam into database.
	   */
	  @FXML
	  private void addExam(){
		  database.addExam(examTitle.getText(), getDate());
		  // Update data in the table.
		  initExam();
		  examTitle.setText("");
	  }
	  /**
	   * Method creates list years for ComboBox. 
	   * @return The array years.
	   */
	  protected static String[] getYear(){
			String[] years = new String[24];
		    int year = 2017;
			for(int i=0; i<years.length; i++){
				years[i]= ""+year+"";
				year++;
			}
			return years;
		}
	  /**
	   * Method creates list mouths for ComboBox. 
	   * @return The array months.
	   */
		protected static String[] getMonth(){
			String[] months = new String[12];
		    int month =1;
			for(int i=0; i<months.length; i++){
				months[i]= ""+month+"";
				month++;
			}
			return months;
		}
		/**
		   * Method creates list days for ComboBox. 
		   * @return The array days.
		   */
		protected static String[] getDay(){
			String[] days = new String[31];
		    int day =1;
			for(int i=0; i<days.length; i++){
				days[i]= ""+day+"";
				day++;
			}
			return days;
		}
		/**
		 * Setting year, month,day and converting them to the format date. 
		 * @return The date in the format "year-month-day"
		 */
		private String getDate(){
			return year.getValue()+"-"+month.getValue()+"-"+day.getValue();
		}
		/**
		 * Method deletes student form database
		 */
		private void deleteStudent(){
			database.deleteStudents(students.getSelectionModel().getSelectedItem().getId());
			// Update table of the students
			initStud();
		}
		/**
		 * Method deletes group from database
		 */
		private void deleteGroup(){
			database.deleteGroup(groups.getSelectionModel().getSelectedItem().getId());
			// Update table of the groups
			initGroup();
			initGroupBox();
		}
		/**
		 * Method deletes exam from database
		 */
		private void deleteExam(){
			database.deleteExam(exams.getSelectionModel().getSelectedItem().getId());
			// Update table of the exams
			initExam();
		}
		/**
		 * Method edits data of the student in database
		 */
		private void editStudent(){
			StudEditController.init(students.getFocusModel().getFocusedItem());
			studEdit = WindowStudEdit.createStudEdit();
			studEdit.initModality(Modality.APPLICATION_MODAL);	
			studEdit.show();
		}
		/**
		 * Method edits data of the group in database
		 */
		private void editGroup(){
			GroupEditController.init(groups.getFocusModel().getFocusedItem());
			groupEdit = WindowGroupEdit.createGroupEdit();
			groupEdit.initModality(Modality.APPLICATION_MODAL);
			groupEdit.show();
		}
		/**
		 * Method edits data of the exam in database
		 */
		
		private void editExam(){
			ExamEditController.init(exams.getFocusModel().getFocusedItem());
			examEdit = WindowExamEdit.createExamEdit();
			examEdit.initModality(Modality.APPLICATION_MODAL);
			examEdit.show();
		}

}
