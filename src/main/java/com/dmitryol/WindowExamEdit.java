package com.dmitryol;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
/**
 * The window for edits students
 * @author Dmitry
 *
 */
class WindowExamEdit {
	
private static final String RES="edit_exam.fxml";
	
	private WindowExamEdit(){
		
	}

	public static Stage createExamEdit(){
		
		
		Stage stage=null;
		try{
			stage = new Stage();
			
			AnchorPane root = FXMLLoader.load(WindowStudEdit.class.getResource(RES));
			Scene scene = new Scene(root);
			
			stage.setScene(scene);
			stage.setTitle("Edit Exam");
			
		}catch(IOException e){
			System.out.println("file "+RES+" not found");
			}
		return stage;
		
	}

}
