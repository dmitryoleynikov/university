package com.dmitryol;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
/**
 * The window for edits exams
 * @author Dmitry
 *
 */

class WindowStudEdit {
	
	
	private static final String RES="edit_stud.fxml";
	
	private WindowStudEdit(){
		
	}

	public static Stage createStudEdit(){
		
		
		Stage stage=null;
		try{
			stage = new Stage();
			
			AnchorPane root = FXMLLoader.load(WindowStudEdit.class.getResource(RES));
			Scene scene = new Scene(root);
			
			stage.setScene(scene);
			stage.setTitle("Edit Student");
			
		}catch(IOException e){
			System.out.println(RES);
			}
		return stage;
		
	}

}
