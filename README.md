# README #

### Step 1 


   For start up project you must have installed **PostgreSQL** database. If database not installed, download and instal **PostgreSQL** from oficcial [site](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads) for your operation system. Greate database **university** with user - **postgres** and password - **qwest123**.

### Step 2

 If you don't have installed **git**, download and instal [git](https://git-scm.com/downloads).

###Step 3

 Instal [maven](http://maven.apache.org/download.cgi) for automatic build project.
 
###Step 4

Open command line and execute command in your local repository:

```
#!git

 git clone https://dmitryoleynikov@bitbucket.org/dmitryoleynikov/university.git

```

command clone project in your local repository.
  Execute command:

```
#!maven

mvn clean package

```